use godot::prelude::*;
use godot::engine::{Control, IControl};

struct Tmp;

#[gdextension]
unsafe impl ExtensionLibrary for Tmp {}

#[derive(GodotClass)]
#[class(base=Control)]
struct Orbit {
}

#[godot_api]
impl IControl for Orbit {
    fn init(base: Base<Control>) -> Self {
        Self{}
    }

    fn get_drag_data(&mut self, position: Vector2) -> Variant {
        let mut x = PackedByteArray::new();
        x.push(0);
        x.push(1);
        x.push(2);
        x.to_variant()
    }

    fn can_drop_data(&self, position: Vector2, data: Variant) -> bool {
        godot_print!("can_drop_data: {}, {:?}", position, data.get_type());
        true
    }

    fn drop_data(&mut self, position: Vector2, data: Variant) {
        godot_print!("drop_data: {}, {}", position, data);
    }
}
